#include <Windows.h>
#include <iostream>
#include <string>
#include "Helper.h"

using std::string;
using std::getline;
using std::cout;
using std::cin;
using std::endl;


typedef int(_cdecl *MYPROC)(LPWSTR);

int main()
{

	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	string str;

	while (true)
	{
		
		cout << ":> ";
		getline(cin, str);
		
		Helper::trim(str);

		if (str._Equal("pwd"))
		{

			TCHAR NPath[MAX_PATH];
			GetCurrentDirectory(MAX_PATH, NPath);
			cout << NPath << endl;
		}
		else if (str.substr(0, str.find(" "))._Equal("cd"))
		{

			SetCurrentDirectory(str.substr(str.find(" ") + 1).c_str());
		}
		else if (str.substr(0, str.find(" "))._Equal("create"))
		{

			CreateFile(str.substr(str.find(" ") + 1).c_str(), GENERIC_ALL, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		}
		else if (str._Equal("ls"))
		{

			WIN32_FIND_DATA data;

			TCHAR NPath[MAX_PATH];
			GetCurrentDirectory(MAX_PATH, NPath);

			HANDLE hFind = FindFirstFile(string(string(NPath) + "\\*").c_str(), &data);
			
			if (hFind != INVALID_HANDLE_VALUE)
			{

				do
				{

					std::cout << data.cFileName << std::endl;
				} while (FindNextFile(hFind, &data));
				
				FindClose(hFind);
			}
		}
		else if (str._Equal("secret"))
		{

			HINSTANCE hintsLib;
			MYPROC ProcAdd;

			hintsLib = LoadLibrary(TEXT("SECRET.dll"));

			if (hintsLib != NULL)
			{

				ProcAdd = (MYPROC)GetProcAddress(hintsLib, "TheAnswerToLifeTheUniverseAndEverything");
				
				if (NULL != ProcAdd)
				{

					cout << ProcAdd(NULL) << endl;
					FreeLibrary(hintsLib);
				}
			}
		}
		else if (str.substr(0, str.find(" "))._Equal("exec"))
		{

			//C:\Users\magshimim\Desktop\mini_project\sqlite3.exe
			string tStr = str.substr(str.find(" ") + 1);
	
			SHELLEXECUTEINFO info;
			info.cbSize = sizeof(SHELLEXECUTEINFO);
			info.lpVerb = "open";
			info.lpFile = tStr.c_str();
			info.lpParameters = 0;
			info.nShow = SW_NORMAL;
			info.fMask = SEE_MASK_NOCLOSEPROCESS;
			info.hwnd = NULL;
			info.lpDirectory = "C:\\";
			
			ShellExecuteEx(&info);
			WaitForSingleObject(info.hProcess, INFINITE);

			cout << "operation exited with code " << GetLastError() << endl;
		}
	}

	system("PAUSE");
	return 0;
}